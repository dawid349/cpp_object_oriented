
#include "types.h"

bool Bankomat::sprawdz_status()
{
    return status;
}

void Bankomat::info()
{
    cout<<"Blad inicjalizacji, nie podano stanu konta"<<endl;
}

Bankomat::Bankomat(double stan_konta_uzytkownika)
{
    stan_konta = stan_konta_uzytkownika;
    stan = IDLE;
    status = OK;
    l_prob = 0;
    stan_konta_przed_op = 0;
    stan_konta_po_op = 0;
}

Bankomat::Bankomat()
{
    this -> info();
    status = ERR;
}

bool Bankomat::reset_strumienia()
{
    bool cin_ok = cin.fail(); // status wprowadzania danych
    cin.clear();              // reset flagi bledu
    cin.ignore(numeric_limits <streamsize>::max(), '\n'); // czyszczenie bufora wejsciowego

    if(cin_ok)
        return true; // wystapil blad
    else
        return false; // wszystko ok
}

void Bankomat::zapis_danych_op(double kwota_op)
{
    if(indeks_hist_op >= 10)
    {
        indeks_hist_op = dlugosc_hist - 1;

        for(int8_t i = 0; i < dlugosc_hist - 1; i++)
        {
            historia_operacji[i] = historia_operacji[i+1];
        }
    }

    SYSTEMTIME st;
    GetSystemTime(&st);
    /* zapis danych ostatniej operacji */
    historia_operacji[indeks_hist_op].data_operacji[ROK] = st.wYear;
    historia_operacji[indeks_hist_op].data_operacji[MIESIAC] = st.wMonth;
    historia_operacji[indeks_hist_op].data_operacji[DZIEN] = st.wDay;
    historia_operacji[indeks_hist_op].data_operacji[GODZINA] = st.wHour;
    historia_operacji[indeks_hist_op].data_operacji[MINUTA] = st.wMinute;
    historia_operacji[indeks_hist_op].data_operacji[SEKUNDA] = st.wSecond;
    historia_operacji[indeks_hist_op].typ_operacji = WYPLATA;
    historia_operacji[indeks_hist_op].kwota = kwota_op;
    historia_operacji[indeks_hist_op].st_konta_przed_op = stan_konta_przed_op;
    historia_operacji[indeks_hist_op].st_konta_po_op = stan_konta_po_op;
    cout<<"stan konta przed: "<<stan_konta_przed_op<<" stan konta po: "<<stan_konta_po_op<<endl;
    indeks_hist_op++;
    l_operacji++;

    if(l_operacji >= 10)
        l_operacji = 10;
}

double Bankomat::wyplata_srodkow()
{
    cout<<"Prosze podac kwote do wyplacenia: "<<endl;
    double kwota;
    cin>>kwota;
    bool cin_status = reset_strumienia(); //jesli wprowadzono bledny typ danych zacznij od nowa

   if(!cin_status)
   {
        if(kwota > stan_konta)
        {
            cout<<"Operacja anulowana z powodu braku wystarczajacej ilosci srodkow na koncie"<<endl<<"nacisnij dowolny klawisz zeby kontynuowac"<<endl;
            getchar();
            return -1;
        }
        if(kwota < 0)
        {
            cout<<"Operacja anulowana z powodu blednie wprowadzonej kwoty"<<endl<<"nacisnij dowolny klawisz zeby kontynuowac"<<endl;
            getchar();
            return -1;
        }

        stan_konta-=kwota;
        return kwota;
   }
   else
   {
       cout<<"Blad wprowadzania danych, nacisnij dowolny przyscisk, aby zaczac od nowa"<<endl;
       return -1;
   }
}

void Bankomat::wyplata_info(kwota_en kwota)
{
    int kwota_2 = 0;
    switch(kwota)
    {
        case K_10_ZL:
            kwota_2 = 10;
        break;

        case K_20_ZL:
            kwota_2 = 20;
        break;

        case K_50_ZL:
            kwota_2 = 50;
        break;

        case K_100_ZL:
            kwota_2 = 100;
        break;

        case K_200_ZL:
            kwota_2 = 200;
        break;

        default:
            kwota_2 = 0;
        break;
    }
    zapis_danych_op(kwota_2);
    cout<<"Dokonano wyplaty srodkow w wysokosci: "<<kwota_2<<endl<<"Aby przejsc dalej nacisnij dowolny klawisz"<<endl;
    getchar();
    stan = WYBOR_OPERACJI;
}

void Bankomat::petla_operacji()
{
    /* Obs³uga logiki bankomatu -- maszyna stanów */

    while(1)
    {
        switch(stan)
        {
            case IDLE:
            {
                cout<< "Wprowadz \"S\" zeby rozpoczac" <<endl;
                char znak;
                cin>>znak;
                bool flag = reset_strumienia();

                if(znak == 'S' && flag == 0)
                {
                    stan = LOGOWANIE;
                }
            }
            break;

            case LOGOWANIE:
            {
                cout<<"Wprowadz kod PIN"<<endl;
                char kod[4];
                cin>>kod;
                bool cin_state = reset_strumienia();
                if(cin_state) // jesli blad wczytywania -- rozpocznij obsluge stanu od nowa
                    break;

                uint8_t kontrola = 0;
                for(uint8_t i = 0; i<4; i++) // sprawdzenie poprawnosci wprowadzonego kodu PIN
                {
                    if(kod[i] != kod_PIN[i])
                         break;

                    else
                        kontrola++;
                }

                if(kontrola == 4)
                {
                    stan = WYBOR_OPERACJI; // PIN poprawny, zmiana stanu
                }
                else
                {
                    l_prob++;       // inkrementacja liczby blednych logowan

                    if(l_prob >= max_l_prob)
                    {
                            l_prob = 0;
                            stan = WPROWADZANIE_KODU_AWARYJNEGO;
                    }
                }
            }
            break;

            case WPROWADZANIE_KODU_AWARYJNEGO:
            {
                cout<<"Przekroczono maksymalna dopuszczalna liczbe prob\n Prosze wprowadzic kod awaryjny"<<endl;
                char kod[4];
                cin>>kod;
                bool cin_status = reset_strumienia();
                if(cin_status)
                    break;

                uint8_t kontrola = 0;

                for(uint8_t i = 0; i<4; i++) // sprawdzenie poprawnosci wprowadzonego kodu PIN
                {
                    if(kod[i] != kod_awaryjny[i])
                         break;

                    else
                        kontrola++;
                }

                if(kontrola == 4)
                {
                    stan = WYBOR_OPERACJI; // PIN poprawny, zmiana stanu
                }
                else
                {
                    l_prob++;       // inkrementacja liczby blednych logowan

                    if(l_prob >= 1)
                    {
                        l_prob = 0;
                        stan = TRYB_SERWISOWY;
                    }
                }

            }
            break;

            case WYBOR_OPERACJI:
            {
                cout<<"Prosze wybrac rodzaj operacji: W - wyplata srodkow, I - dostepne srodki, H - historia operacji, X - zakoncz"<<endl;
                char operacja;
                cin>>operacja;
                bool cin_status = reset_strumienia();

                if(cin_status)
                    break;

                if(operacja == 'I')
                {
                    stan = PODGLAD_STANU_KONTA;
                }
                else if(operacja == 'W')
                {
                    stan = WYPLATA_PIENIEDZY;
                }
                else if(operacja == 'X')
                {
                    stan = KONIEC;
                }
                else if(operacja == 'H')
                {
                    stan = PODGLAD_HISTORII_OPERACJI;
                }
            }
            break;

            case TRYB_SERWISOWY:
            {
                cout<<"Niepoprawny kod awaryjny, prosze skontaktowac sie z serwisem\n"<<endl;
                cout<<"Aby przejsc dalej podaj kod sewisanta"<<endl;
                char kod[4];
                cin>>kod;
                bool cin_status = reset_strumienia();
                if(cin_status)
                    break;

                uint8_t kontrola = 0;

                for(uint8_t i = 0; i<4; i++) // sprawdzenie poprawnosci wprowadzonego kodu serwisanta
                {
                    if(kod[i] != kod_serwisanta[i])
                         break;

                    else
                        kontrola++;
                }

                if(kontrola == 4)
                {
                    stan = IDLE; // kod serwisanta poprawny, zmiana stanu
                }
                else
                {
                    l_prob++;       // inkrementacja liczby blednych logowan

                    if(l_prob >= 3)
                    {
                        l_prob = 0;
                        stan = TRYB_SERWISOWY;
                    }
                }
            }
            break;

            case PODGLAD_STANU_KONTA:
            {
                cout<<"Aktualny stan twojego konta wynosi: "<<stan_konta<<endl;
                cout<<"M - powrot do menu glownego"<<endl;
                char komenda;
                cin>>komenda;
                bool cin_status = reset_strumienia();
                if(cin_status)
                    break;

                if(komenda == 'M')
                    stan = WYBOR_OPERACJI;
            }
            break;

            case WYPLATA_PIENIEDZY:
            {
                cout<<"Wybierz kwote do wyplacenia: "<<endl;
                cout<<K_10_ZL<<" - 10zl " <<K_20_ZL<<" - 20zl "<<K_50_ZL<<" - 50zl "<<endl<<K_100_ZL<<" - 100zl "<<K_200_ZL<<" - 200zl "<<INNA<<" - Inna kwota "<<endl;
                uint8_t kwota = MAX_KWOTA;
                cin>>kwota;
                bool cin_status = reset_strumienia();
                if(cin_status)
                    break;

                kwota-=ASCII_OFFSET;

                stan_konta_przed_op = stan_konta;

                switch(kwota)
                {
                    case K_10_ZL:
                    {
                        stan_konta-=10;
                        stan_konta_po_op = stan_konta;
                        wyplata_info(K_10_ZL);
                    }
                    break;

                    case K_20_ZL:
                    {
                        stan_konta-=20;
                        stan_konta_po_op = stan_konta;
                        wyplata_info(K_20_ZL);
                    }
                    break;

                    case K_50_ZL:
                    {
                        stan_konta-=50;
                        stan_konta_po_op = stan_konta;
                        wyplata_info(K_50_ZL);
                    }
                    break;

                    case K_100_ZL:
                        {
                            stan_konta-=100;
                            stan_konta_po_op = stan_konta;
                            wyplata_info(K_100_ZL);
                        }
                    break;

                    case K_200_ZL:
                        {
                            stan_konta-=200;
                            stan_konta_po_op = stan_konta;
                            wyplata_info(K_200_ZL);
                        }
                    break;

                    case INNA:
                        {
                            double kwota = wyplata_srodkow();
                            if(kwota == -1)
                            {
                                kwota = 0;
                            }
                            stan_konta_po_op = stan_konta;
                            zapis_danych_op(kwota);
                            cout<<"Dokonano wyplaty srodkow w wysokosci: "<<kwota<<endl<<"Aby przejsc dalej nacisnij dowolny klawisz"<<endl;
                            getchar();
                            stan = WYBOR_OPERACJI;
                        }
                    break;
                }
            }
            break;

            case KONIEC:
            {
                cout<<"Dziekujemy za skorzystanie z naszego urzadzenia"<<endl;
                cout<<"Nacisnij dowolny klawisz zakonczyc sesje"<<endl;
                getchar();
                stan = IDLE;
            }
            break;

            case PODGLAD_HISTORII_OPERACJI:
            {
                cout<<"Lista 10 ostatnich operacji: "<<endl<<endl;
                cout<<"liczba op: "<<(int)l_operacji<<endl<<"index hist: "<<(int)indeks_hist_op<<endl;

                for(int8_t i = l_operacji - 1; i >= 0; i--)
                {
                    if(historia_operacji[i].typ_operacji == WYPLATA)
                    {
                        cout<<(int)(l_operacji-i)<<". "<<"data: "<<historia_operacji[i].data_operacji[DZIEN]<<"."<<historia_operacji[i].data_operacji[MIESIAC]<<" ,g. "<<historia_operacji[i].data_operacji[GODZINA]<<":"<<historia_operacji[i].data_operacji[MINUTA]<<"; Typ operacji: Wyplata"<<"; kwota: "<<historia_operacji[i].kwota<<endl;
                    }
                }

                cout<<endl<<endl<<"M - powrot do menu glownego"<<endl;
                char komenda;
                cin>>komenda;
                bool cin_status = reset_strumienia();
                if(cin_status)
                    break;

                if(komenda == 'M')
                    stan = WYBOR_OPERACJI;

            }
            break;

            default:
                status = ERR;  // jesli urzadzenie w niepozadanym stanie ustaw status bledu
            break;
        }

    }
}
