#include <string.h>
#include <stdint.h>
#include <iostream>
#include <limits>
#include <windows.h>
#include <winbase.h>

#define ASCII_OFFSET 48

using namespace std;

typedef enum
{
    IDLE,
    LOGOWANIE,
    WYBOR_OPERACJI,
    WYBOR_KWOTY,
    WPROWADZANIE_KODU_AWARYJNEGO,
    PODGLAD_STANU_KONTA,
    WYPLATA_PIENIEDZY,
    TRYB_SERWISOWY,
    PODGLAD_HISTORII_OPERACJI,
    KONIEC

}stan_pracy;

typedef enum
{
    ROK,
    MIESIAC,
    DZIEN,
    GODZINA,
    MINUTA,
    SEKUNDA,
    MAX_INDEKS

}indeks_en;

typedef enum
{
    BRAK_OP,
    WYPLATA,
    MAX_OP

}operacja_en;

typedef enum
{
    OK = 1,
    ERR = 0

}status_en;

typedef struct
{
    int data_operacji[MAX_INDEKS];
    double kwota;
    operacja_en typ_operacji;
    double st_konta_przed_op;
    double st_konta_po_op;

}dane_operacji_t;

typedef enum
{
        K_10_ZL,
        K_20_ZL,
        K_50_ZL,
        K_100_ZL,
        K_200_ZL,
        INNA,
        MAX_KWOTA

}kwota_en;

class Bankomat{

    private:
        double stan_konta;
        const static uint8_t dlugosc_hist = 10;
        const char kod_PIN[4] = {'1', '2', '3', '4'};
        const char kod_awaryjny[4] = {'4', '3', '2', '1'};
        const char kod_serwisanta[4] = {'7', '7', '7', '7'};
        dane_operacji_t historia_operacji[dlugosc_hist];
        bool status;
        const uint8_t max_l_prob = 3;
        uint8_t indeks_hist_op = 0;
        int32_t l_operacji = 0;

        void zapis_danych_op(double kwota_op);

    public:
        bool sprawdz_status();
        void info();
        Bankomat(double stan_konta_uzytkownika);
        Bankomat();
        void petla_operacji();
        stan_pracy stan;
        bool reset_strumienia();
        bool login();
        double wyplata_srodkow();
        void wyplata_info(kwota_en kwota);

        uint8_t l_prob;
        double stan_konta_przed_op;
        double stan_konta_po_op;
};
